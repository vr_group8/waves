using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeavyCenter : MonoBehaviour
{
    // Start is called before the first frame update
    RectTransform center;
    public float fixed_scale = 1;
    void Start()
    {
        center = this.GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void graphic(float x , float y , float z , float w){ //top right , left , bottom right , left
        // horizontal 150-15 = 135 vertical 300-15 = 285
        print(x + " and " + y + " and " + z + " and " + w);
        float posx = (x*1 + y*(-1) + z*1 + w*(-1));
        float posy = (x*1 + y*1 + z*(-1) + w*(-1));
        if(posx > 150/fixed_scale) posx = 150/fixed_scale;
        if(posx < -150/fixed_scale) posx = -150/fixed_scale;
        if(posy > 100/fixed_scale) posy = 100/fixed_scale;
        if(posy < -100/fixed_scale) posy = -100/fixed_scale;
        center.anchoredPosition = new Vector2(posx , posy)* fixed_scale;
        print(center.anchoredPosition);

    }
    
}
